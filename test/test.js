$(function () {
	var output = $('#output');

	$().lytt({
		success: function (result) {
			output.val(result);
		},
		error: function (error) {
			output.val(error);	
		},
		DEBUG: true,
		language: 'it' // Italian
	});

	$('html').keypress(function (event) {
		if (event.which === 97) { // 'a' pressed
			$().lytt('listen');
		}
	});
});