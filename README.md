# Lytt - Speech recognition in Chrome Apps
Lytt (Norwegian for "listen") is a jQuery plugin to enable speech recognition in Google Chrome's apps.

## Installation
To use Lytt in your apps follow those steps:

1. Add *experimental* support in you `manifes.json` file
1. Enable the experimental API in Chrome. Go to `chrome://flags`, find "Experimental Extension APIs", click its "Enable" link, and restart Chrome.
1. Require `jquery.js` and `lytt.js` in your HTML file.

## Usage

You can interact with Lytt using those methods.

### $().lytt(options)
Method used to initialize Lytt. The `options` object may contain those values:

* `DEBUG:boolean` default `false`, if `true` enable console messages
* `language:string` default `"en"`, BCP-47 language code of the language to recognize
* `success:function(result:string)` default `null`, function called if a speech is recognized
* `error:function(error:string)` default `null`, function called if an error occours

### $().lytt('listen')
This method must be invoked after `$().lytt(options)`. Try to caputre a speech and calls the appropriate callback. Also shows a feedback div and destroy it when it finish.

## Style
Lytt creates a div with id `lytt_feedback`. The div also has a class that changes during the speech recognition.

* `listening`: the browser is ready to capture audio
* `audioStart`: the system starts to detect sound
* `audioEnd`: the system detects enough silence to consider the ongoing speech has ended
* `success`: speech recognized
* `error`: if an error occours