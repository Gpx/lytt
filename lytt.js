/*
 * jQuery's Lytt plugin
 * Created by: Giorgio Polvara aka Gpx - polvara@gmail.com
 * Copyright: Trizero S.r.l.
 * Version: 1.0.0
*/

(function ($) {
	var settings,
		constants = {
			messages: {
				errors: {
					no_chrome: 'jQuery.lytt must be executed in a Google Chrome app',
					no_api: 'jQuery.lytt needs speechInput APIs to work',
					generic_error: 'Lytt error: '
				},
				infos: {
					result: 'Lytt: speech recognition terminated',
					on_sound_start: 'Lytt: sound detected',
					on_sound_end: 'Lytt: silence detected'
				}
			},
			feedback_id: 'lytt_feedback',
			classes: {
				listening: 'listening',
				success: 'success',
				error: 'error',
				soundStart: 'sound_start',
				soundEnd: 'sound_end'
			}
		},
		methods = {
		/*
		 * Initialize the system
		*/
		init: function (options) {
			/*
			 * Default settings
			*/
			settings = $.extend({
				DEBUG: false,	// displya or not debug messages
				language: 'en',		// language to recognize
				success: null,	// function called when a speech is detected 
				error: null 	// funciton called when an error occours
			}, options);

			// Check if we are in a Google Chrome app
			if (typeof chrome !== 'undefined' && chrome !== null) {
				// Check if speechInput APIs exists
				if (typeof chrome.speechInput !== 'undefined' && chrome.speechInput !== null) {
					prepareCatchSpeech();
				}
				// Fallback to experimental APIs
				else if (typeof chrome.experimental !== 'undefined' && chrome.experimental !== null) {
					// Check if experimental speechInput APIs exists
					if (typeof chrome.experimental.speechInput !== 'undefined' && chrome.experimental.speechInput !== null) {
						remapApis();
						prepareCatchSpeech();
					} else {
						$.error(constants.messages.errors.no_api);
					}
				} else {
					$.error(constants.messages.errors.no_api);
				}
			} else {
				$.error(constants.messages.errors.no_chrome);
			}
		},

		/*
		 * Record a speech an convert it into a String
		*/
		listen: function () {
			startCatchSpeech();
		}
	},

	/*
	 * Map experimental APIs to official APIs
	*/
	remapApis = function () {
		chrome.speechInput = chrome.experimental.speechInput;
	},

	/*
	 * Set listeners
	*/
	prepareCatchSpeech = function () {
		chrome.speechInput.onError.addListener(onError);
		chrome.speechInput.onResult.addListener(onResult);
		chrome.speechInput.onSoundStart.addListener(onSoundStart);
		chrome.speechInput.onSoundEnd.addListener(onSoundEnd);
	},

	/*
	 * Check if another section is active and close it
	*/
	onIsRecording = function (isRecording) {
		if (isRecording) {
			chrome.speechInput.stop(catchSpeech);
		} else {
			catchSpeech();
		}
	},

	/*
	 * Precheck before start recording
	*/
	startCatchSpeech = function () {
		// Check if another section is active
		chrome.speechInput.isRecording(onIsRecording);
	},

	/*
	 * Display feedback and start recording
	*/
	catchSpeech = function () {
		showFeedback();
		
		chrome.speechInput.start({
			language: settings.language
		}, onStart);
	},

	/*
	 * Display the feedback element
	*/
	showFeedback = function () {
		var feedback;

		destroyFeedback();
		
		feedback = $('<div id="' + constants.feedback_id + '"></div>');
		feedback.appendTo('body');
	},

	/*
	 * Return the feedback element
	*/
	getFeedback = function () {
		return $('#' + constants.feedback_id);
	},

	/*
	 * If feedback element exists destroy it
	*/
	destroyFeedback = function () {
		var feedback = getFeedback();
		if (typeof feedback !== 'undefined' && feedback !== null) {
			$(feedback).remove();
		}
	},

	/*
	 * Set appropriate class on feedback element
	*/
	onStart = function () {
		var feedback = getFeedback();
		feedback.addClass(constants.classes.listening);
	},

	/*
	 * Set appropriate class on feedback element and invoke the callback function
	*/
	onError = function (error) {
		var errorCode = error.code,
			feedback = getFeedback();

		if (settings.DEBUG) {
			console.error(constants.messages.errors.generic_error + errorCode);
		}

		feedback
			.removeClass()
			.addClass(constants.classes.error)
			.fadeOut(3000);

		if (typeof settings.error !== 'undefined' && settings.error !== null) {
			settings.error(errorCode);
		}
	},

	/*
	 * Delete feedback element and invoke the callback function passing the speech
	*/
	onResult = function (event) {
		if (settings.DEBUG) {
			console.info(constants.messages.infos.result, event);
		}

		var feedback = getFeedback();
		feedback
			.removeClass()
			.addClass(constants.classes.success)
			.fadeOut(3000);

		var result = event.hypotheses[0].utterance;
		settings.success(result);
	},

	/*
	 * Change class to feedback element
	*/
	onSoundStart = function () {
		if (settings.DEBUG) {
			console.info(constants.messages.infos.on_sound_start);
		}

		var feedback = getFeedback();
		feedback
			.removeClass()
			.addClass(constants.classes.soundStart);
	},

	/*
	 * Change class to feedback element
	*/
	onSoundEnd = function () {
		if (settings.DEBUG) {
			console.info(constants.messages.infos.on_sound_end);
		}

		var feedback = getFeedback();
		feedback
			.removeClass()
			.addClass(constants.classes.soundEnd);
	};
	
	$.fn.lytt = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method ) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.lytt');
		}
	};
})(jQuery);